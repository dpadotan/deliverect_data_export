
# imports
import time
from datetime import datetime, date, timedelta
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from configparser import ConfigParser
import logging
import pandas as pd

# Reading in config params
parser = ConfigParser()
parser.read('config.ini')
driverPath = parser['vars'].get('chrome_webdriver')
authEmail = parser['vars'].get('auth_email')
pw = parser['vars'].get('pw')
exportBackdate = parser['vars'].get('exportBackdate')
userAccountsList = parser['vars'].get('accountsList').split(',')
scrape_flag = parser['vars'].get('scrape')
download_pg_start = parser['vars'].get('download_pg_start')

# Setting up logger
logFormat = '%(asctime)s | %(filename)s | Line no: %(lineno)d | %(levelname)s: %(message)s'
logging.basicConfig(filename='deliverect_export.log', level=logging.INFO, format=logFormat, datefmt='%Y-%m-%d %H:%M:%S')
logging.info('LOGGING START')

# First date to start export from
exportStartDate = datetime.strptime(exportBackdate, '%Y-%m-%d').date()
todayDate = date.today()

# Initialising an instance of the web driver + an explicit wait
driver = webdriver.Chrome(driverPath)
driver.implicitly_wait(10)
driver.get("https://frontend.deliverect.com/")
wait = WebDriverWait(driver, 10)

dl_metadata = []
metaData = []

try:

    #----- Log in to Deliverect account with Google details -----#
    googleAuthButton = driver.find_element_by_xpath('//*[@id="login-page-container"]//'
                                                    'a[@class="auth0-lock-social-button auth0-lock-social-big-button" '
                                                    'and @data-provider="google-oauth2"]')
    googleAuthButton.click()

    emailField = driver.find_element_by_xpath('//*[@id="identifierId"]')
    emailField.send_keys(authEmail)

    nextButton = driver.find_element_by_xpath('//*[@id="identifierNext"]/div/button')
    nextButton.click()

    pwField = driver.find_element_by_xpath('//*[@id="password"]/div[1]/div/div[1]/input')
    pwField.send_keys(pw)

    nextButton_pw = driver.find_element_by_xpath('//*[@id="passwordNext"]/div/button')
    nextButton_pw.click()

    #---------------------------------------------------------------#

    try:
        time.sleep(5)
        cookieAcceptButton = driver.find_element_by_xpath('//*[@id="cookie-banner-content"]//'
                                                          'span[contains(text(), "Accept Cookies")]')
        cookieAcceptButton.click()

    finally:
        if scrape_flag == 'y':
            time.sleep(3)

            # Navigate to the accounts page
            accountsMenu = driver.find_element_by_xpath('//*[@id="root"]//a[@href="/accounts"]')
            accountsMenu.click()
            time.sleep(3)

            # Determining the total number of pages on the Accounts page
            numPages = len(driver.find_elements_by_xpath('//*[@class="list-pagination-number-label"]'))

            for pg in range(1, numPages+1):
                pgButton = driver.find_element_by_xpath(f'//a[@class="list-pagination-number-label" and text()="{pg}"]')
                pgButton.click()

                time.sleep(5)

                # Retrieving list of all account names
                accountTextList = driver.find_elements_by_xpath('//*[@id="content-wrapper"]//div[contains(@class, '
                                                                '"locations-list-item-info-container")]//span[contains(@class, "notranslate")]')

                acctsTxt = [x.text for x in accountTextList]

                # If user-defined list of accounts is set to 'All' then loop over all the accounts on the page
                if "All" in userAccountsList:
                    accountIndices = range(len(accountTextList))
                else:
                    accountIndices = [i for i, x in enumerate(accountTextList) if x.text in userAccountsList]

                for acct in accountIndices:

                    # Switching to a specific account
                    accountsList = driver.find_elements_by_xpath('//*[contains(text(), "Switch To Account")]')
                    accountsList[acct].click()

                    time.sleep(8)

                    # Navigate to the orders page
                    ordersMenu = driver.find_element_by_xpath('//*[@id="root"]//a[@href="/orders"]')
                    ordersMenu.click()

                    filterButton = driver.find_element_by_xpath('//*[@id="content-wrapper"]//span[contains(text(), "Filter")]')
                    filterButton.click()

                    startDateField = driver.find_element_by_xpath('//*[@id="startDate"]')
                    startDateField.click()

                    # Converting the month to start the export into a date format
                    exportStartMonth = date(exportStartDate.year, exportStartDate.month, 1)

                    downloadDate = exportStartDate

                    while True:
                        time.sleep(3)
                        # Reading the current month displayed on the calendar and converting to a date format
                        calendarMonths = driver.find_elements_by_xpath(
                            '//*[@id="content-wrapper"]//div[contains(@class, "CalendarMonth_caption")]')
                        firstVisibleMMYY = calendarMonths[1].text
                        firstCalendarDate = datetime.strptime(firstVisibleMMYY, '%B %Y')
                        calendarMonth = date(firstCalendarDate.year, firstCalendarDate.month, 1)

                        if calendarMonth == exportStartMonth:
                            break
                        else:
                            monthBackButton = driver.find_element_by_xpath('//*[@id="content-wrapper"]//'
                                                                           'div[@aria-label="Move backward to switch to the previous month."]')
                            monthBackButton.click()
                            time.sleep(3)

                    while downloadDate < todayDate:

                        time.sleep(3)

                        startDateField = driver.find_element_by_xpath('//*[@id="startDate"]')
                        startDateField.click()

                        # Reading the current month displayed on the calendar and converting to a date format
                        calendarMonths = driver.find_elements_by_xpath(
                            '//*[@id="content-wrapper"]//div[contains(@class, "CalendarMonth_caption")]')
                        firstVisibleMMYY = calendarMonths[1].text
                        firstMM = firstVisibleMMYY.split(' ')[0]
                        firstYY = firstVisibleMMYY.split(' ')[1]
                        firstCalendarDate = datetime.strptime(firstVisibleMMYY, '%B %Y')
                        calendarMonth = date(firstCalendarDate.year, firstCalendarDate.month, 1)
                        dlMonth = downloadDate.replace(day=1)

                        if dlMonth != calendarMonth:
                            firstVisibleMMYY = calendarMonths[2].text
                            firstMM = firstVisibleMMYY.split(' ')[0]
                            firstYY = firstVisibleMMYY.split(' ')[1]
                            monthForwardButton = driver.find_element_by_xpath('//*[@id="content-wrapper"]//'
                                                                           'div[@aria-label="Move forward to switch to the next month."]')
                            monthForwardButton.click()

                        # Clicking the same date twice sets it as both the start date and end date on the calendar
                        for i in range(2):

                            calendarDate = driver.find_element_by_xpath(f'//td[contains(@class, "CalendarDay") '
                                                                        f'and @role="button" '
                                                                        f'and text()="{downloadDate.day}" '
                                                                        f'and contains(@aria-label, "{firstMM}") and '
                                                                        f'contains(@aria-label, "{firstYY}")]')
                            driver.execute_script("arguments[0].click();", calendarDate)

                        try:
                            time.sleep(3)
                            # Checking if there are no orders for this date
                            driver.find_element_by_xpath('//*[@id="content-wrapper"]//div[contains(@class, "empty-page-container")]')
                            print('No orders')
                            metaData.append([acctsTxt[acct], downloadDate, 0])
                        except NoSuchElementException:
                            print('Orders present')
                            metaData.append([acctsTxt[acct], downloadDate, 1])

                            moreButton = driver.find_element_by_xpath('//*[@id="content-wrapper"]//span[contains(@class, "button") and '
                                                                      'text()="more"]')
                            moreButton.click()

                            exportButton = driver.find_element_by_xpath('//*[@id="content-wrapper"]//div[contains(text(), "Export orders")]')
                            exportButton.click()

                            time.sleep(3)

                            loopCt = 0

                            # Selecting all data fields to be exported
                            while True:

                                # Getting the text indicating how many of the total number of fields have been added to the report to be exported
                                fieldsCount = driver.find_element_by_xpath('//*[@class="export-number-selected-wrapper"]')
                                fieldsCountText = fieldsCount.text

                                if ('32 of 32' in fieldsCountText) | (loopCt > 20):
                                    break

                                fieldsContainer = driver.find_element_by_xpath('/html/body/div[4]/div/div/div/div[2]/div[1]/div[2]/div')
                                fieldsContainer.click()

                                # Adding columns to the list to be exported
                                fieldInput = driver.find_element_by_xpath(
                                    f'/html/body/div[4]/div/div/div/div[2]/div[1]/div[2]/div/div[1]/div[{loopCt+21}]/div/input')
                                fieldInput.send_keys(Keys.SPACE)
                                fieldInput.send_keys(Keys.SPACE)

                                # Break out of the loop when all the fields have been added to the report or the loop has exceeded a defined number
                                # of iterations (to prevent infinite loops)
                                loopCt += 1

                            # Download the orders report
                            downloadButton = driver.find_element_by_xpath('//span[contains(@class, "button") and contains(text(), "Request Export")]')
                            downloadButton.click()

                            time.sleep(3)

                        downloadDate += timedelta(days=1)


                    # Navigate to the accounts page
                    accountsMenu = driver.find_element_by_xpath('//*[@id="root"]//a[@href="/accounts"]')
                    accountsMenu.click()
                    time.sleep(3)

                    # Returning to the relevant page number in the Accounts menu.
                    # NB: By default when the Accounts menu is selected, it returns to the first page.
                    pgButton = driver.find_element_by_xpath(f'//a[@class="list-pagination-number-label" and text()="{pg}"]')
                    pgButton.click()

    # ---------------------Downloading the report .csv files--------------------- #

    time.sleep(5)

    # Navigate to the operation reports page
    operationReportsMenu = driver.find_element_by_xpath('//*[@id="root"]//a[@href="/operationreports"]')
    operationReportsMenu.click()

    time.sleep(5)

    empty_page = driver.find_elements_by_xpath('//*[@id="content-wrapper"]//'
                                               'div[contains(@class, "empty-page-container")]')

    if len(empty_page) == 0:

        filterButton = driver.find_element_by_xpath('//*[@id="content-wrapper"]//span[contains(text(), "Filter")]')
        filterButton.click()

        operation_type_dropdown = driver.find_element_by_xpath('//*[@id="content-wrapper"]'
                                                               '/main/div/div[1]/div[2]/div/div[1]/div/div/div/div[2]')
        operation_type_dropdown.click()

        export_option = driver.find_element_by_xpath('//div[contains(@id, "react-select") and text()="Export"]')
        export_option.click()

    try:
        reset_filter_button = driver.find_element_by_xpath('//*[@id="content-wrapper"]//span[text() = "Reset The Filters"]')
        reset_filter_button.click()
    except NoSuchElementException:
        pass

    time.sleep(6)

    num_report_pages = len(driver.find_elements_by_xpath('//*[@class="list-pagination-number-label"]'))

    pg_count = int(download_pg_start)

    while True:

        current_page_element = driver.find_element_by_xpath('//*[@class="list-pagination-number-label" and '
                                                            'contains(@aria-label, "your current page")]')

        current_pg_num = int(current_page_element.text)

        if pg_count > 1 and current_pg_num == 1:
            for i in range(pg_count-1):
                next_button = driver.find_element_by_xpath('//*[@id="content-wrapper"]//'
                                                           'li[@class="list-pagination-next-container"]//'
                                                           'i[contains(@class, "chevron-right")]')
                next_button.click()

            current_page_element = driver.find_element_by_xpath('//*[@class="list-pagination-number-label" and '
                                                                'contains(@aria-label, "your current page")]')

            current_pg_num = int(current_page_element.text)

        if current_pg_num != pg_count:
            break

        export_items = driver.find_elements_by_xpath('//*[@id="content-wrapper"]//span[contains(@class, "EXPORT")]')

        for i, e_item in enumerate(export_items):

            export_status_field = e_item.find_element_by_xpath('../../div[contains(@class, '
                                                                 '"statusCode-container")]/div/span')
            export_status_text = export_status_field.text

            account_field = e_item.find_element_by_xpath('../../div[contains(@class, "account-container")]')
            account_text = account_field.text

            operation_start_field = e_item.find_element_by_xpath('../../div[contains(@class, '
                                                                 '"operationStart-container")]')
            operation_start_text = operation_start_field.text

            if export_status_text == 'SUCCESS':
                e_item.click()
                time.sleep(3)
                download_link_button = e_item.find_element_by_xpath('../../../div[contains(@class, "ReactCollapse")]'
                                                                    '//button//span[text()="Get Download Link"]')
                download_link_button.click()

                time.sleep(2)

                download_csv_button = e_item.find_element_by_xpath('../../../div[contains(@class, "ReactCollapse")]'
                                                                   '//button//span[text()="Download CSV"]')
                download_csv_button.click()

                time.sleep(3)

            dl_metadata.append([operation_start_text, account_text, export_status_text])

        # pg_view_count
        next_button = driver.find_element_by_xpath('//*[@id="content-wrapper"]//'
                                                   'li[@class="list-pagination-next-container"]//'
                                                   'i[contains(@class, "chevron-right")]')
        next_button.click()

        time.sleep(6)

        pg_count += 1

finally:
    driver.quit()
    df_1 = pd.DataFrame(metaData, columns=['Account', 'Date', 'Orders_Present'])
    df_1.to_csv('metadata_out.csv', index=False)

    df_2 = pd.DataFrame(dl_metadata, columns=['Operation Start', 'Account', 'Status'])
    df_2.to_csv('export_metadata.csv', index=False)
