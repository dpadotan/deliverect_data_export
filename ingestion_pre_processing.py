import pandas as pd
import os
from datetime import datetime
import time
import logging

# Setting up logger
logFormat = '%(asctime)s | %(filename)s | Line no: %(lineno)d | %(levelname)s: %(message)s'
logging.basicConfig(filename='deliverect_pre_processing_log.log', level=logging.INFO, format=logFormat, datefmt='%Y-%m-%d %H:%M:%S')
logging.info('LOGGING START')

startTime = time.time()

inputDIR = "C:/Users/DeroshanPadotan/Documents/Deliverect Data/"
outputDIR = "C:/Users/DeroshanPadotan/Documents/Deliverect Data Export/Formatted_Deliverect_Data_2/"

dirName = inputDIR.split('/')[-2]
errorMsgs = ["Invalid", "Fetching Uber order failed"] #List of error messages detected in field 18 (Failure Message)

try:
    err_metadata = []
    metadatacols = ["Directory", "File", "Line_Num", "Tot_Num_Fields", "Failure_Msg"]

    for file in os.listdir(inputDIR):
        # Declaring a list to store all lines from the file after any fixes have been applied
        outputDataList = []
        with open(inputDIR + file, encoding="utf8") as f:
            for i, line in enumerate(f):
                lineList = line.split('\t')

                if i == 0:
                    colNames = lineList
                else:
                    if any(msg in lineList[17] for msg in errorMsgs) and len(lineList) > 30:
                        numFieldsDel = len(lineList) - 30  # The number of fields to delete
                        fixedLine = lineList[0:18] + lineList[18 + numFieldsDel:]
                        outputDataList.append(fixedLine)
                        err_metadata.append([dirName, file, i+1, len(lineList), lineList[17]])
                    else:
                        outputDataList.append(lineList)

        out_df = pd.DataFrame(outputDataList, columns=colNames)
        out_df.to_csv(f'{outputDIR}{file[0:-4]}_{dirName}_{str(datetime.today().strftime("%m_%d_%Y %H_%M_%S_%f"))}.csv',
                      index=False, quoting=1)

    meta_df = pd.DataFrame(err_metadata, columns=metadatacols)
    meta_df.to_csv("error_line_metadata.csv", index=False)

except Exception as e:
    logging.error(e, exc_info=True)
    logging.error(f'Failed on file: {file}, line: {i+1}')
finally:
    runTime = time.time() - startTime
    logging.info( f'Total Time Taken: {runTime // 3600} hours {(runTime % 3600) // 60} min {round((runTime % 3600) % 60, 3)} seconds')
    logging.info('LOGGING END')